package main

import (
	"database/sql"
	"fmt"
	"gitee.com/LCGYL/go-study/src/orm/mysql/entity"
	"time"
)

// 定义一个全局对象db
var db *sql.DB

// 定义一个初始化数据库的函数
func initDB() (err error) {
	dsn := "root:Cy520==1314@tcp(rm-bp140715cq976s8j7xo.mysql.rds.aliyuncs.com:3306)/go_db?charset=utf8mb4&parseTime=True"
	// 不会校验账号密码是否正确
	// 注意！！！这里不要使用:=，我们是给全局变量赋值，然后在main函数中使用全局变量db
	db, err = sql.Open("mysql", dsn)
	if err != nil {
		return err
	}
	// 尝试与数据库建立连接（校验dsn是否正确）
	err = db.Ping()
	if err != nil {
		return err
	}
	// 最大连接时长
	db.SetConnMaxLifetime(time.Minute * 3)
	// 最大连接数
	db.SetMaxOpenConns(10)
	// 空闲连接数
	db.SetMaxIdleConns(10)
	return nil
}

func init() {
	err := initDB()
	if err != nil {
		return
	}
}

// QueryRowDemo 查询一条用户数据
func QueryRowDemo(sqlStr string) {
	var u entity.User
	// 确保QueryRow之后调用Scan方法，否则持有的数据库链接不会被释放
	err := db.QueryRow(sqlStr, 1).Scan(&u.Id, &u.Username, &u.Password)
	if err != nil {
		fmt.Printf("scan failed, err:%v\n", err)
		return
	}
	fmt.Printf("id:%d name:%s age:%s\n", u.Id, u.Username, u.Password)
}

// QueryMultiRow 查询多条数据示例
func QueryMultiRow(sqlStr string) {
	rows, err := db.Query(sqlStr, 0)
	if err != nil {
		fmt.Printf("query failed, err:%v\n", err)
		return
	}
	// 非常重要：关闭rows释放持有的数据库链接
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
			panic(err)
		}
	}(rows)

	// 循环读取结果集中的数据
	for rows.Next() {
		var u entity.User
		err := rows.Scan(&u.Id, &u.Username, &u.Password)
		if err != nil {
			fmt.Printf("scan failed, err:%v\n", err)
			return
		}
		fmt.Printf("id:%d username:%s password:%s\n", u.Id, u.Username, u.Password)
	}
}
