package main

import (
	_ "github.com/go-sql-driver/mysql"
)

func main() {
	QueryRowDemo("select id, username, password from user_tbl where id=?")
	QueryMultiRow("select id, username, password from user_tbl where id > ?")
}
