package main

import (
	"context"
	"gitee.com/LCGYL/go-study/src/grpc/proto/grpc-proto-gen/grpc_proto"
	"google.golang.org/grpc"
	"log"
	"net"
)

type server struct {
	grpc_proto.UnimplementedEventServiceServer
	count int
}

func (s *server) Process(ctx context.Context, event *grpc_proto.Event) (*grpc_proto.Event, error) {
	log.Printf("get event Payload: %v", event.Payload)
	g := &grpc_proto.Event{}
	event.Payload = "get " + event.Payload
	return g, nil
}

func (s *server) Consume(consumeServer grpc_proto.EventService_ConsumeServer) error {
	return nil
}

func main() {
	listen, err := net.Listen("tcp", "192.168.110.241:18888")
	if err != nil {
		log.Fatalf("failed to listen: %v \n", err)
	}
	s := grpc.NewServer()
	grpc_proto.RegisterEventServiceServer(s, &server{})
	log.Printf("server listening at==> %v \n", listen.Addr())
	if err := s.Serve(listen); err != nil {
		log.Fatalf("failed to serve: %v \n", err)
	}
}
