package main

import (
	"context"
	"encoding/json"
	"fmt"
	"gitee.com/LCGYL/go-study/src/grpc/proto/grpc-proto-gen/grpc_proto"
	"google.golang.org/grpc"
	"log"
	"sync"
	"time"
)

type GrpcSink struct {
	client      *grpc.ClientConn
	eventClient grpc_proto.EventServiceClient
	chanMap     map[int]chan *grpc_proto.Event
	gw          sync.WaitGroup
}

func (g *GrpcSink) InitChan(ctx context.Context) {
	for i := 0; i < 10; i++ {
		event := make(chan *grpc_proto.Event, 10000)
		g.chanMap[i] = event
		go g.runChan(i, ctx, event)
	}
}

func (g *GrpcSink) runChan(i int, ctx context.Context, events chan *grpc_proto.Event) {
	index := i
	for {
		select {
		case event := <-events:
			log.Printf("chan[%v] use", index)
			_, err := g.eventClient.Process(ctx, event)
			if err != nil {
				log.Fatalf("grpc: sendData fail with error: %v \n ", err)
			}
			g.gw.Done()
		case <-ctx.Done():
			log.Printf("sink node %s instance %d stops data resending \n")
			return
		default:
		}
	}
}

func (g *GrpcSink) initConnect() (err error) {
	g.client, err = grpc.Dial("127.0.0.1:18888", grpc.WithInsecure())
	if err != nil {
		return err
	}
	g.eventClient = grpc_proto.NewEventServiceClient(g.client)
	g.chanMap = make(map[int]chan *grpc_proto.Event, 100)
	return nil
}

func main() {
	ctx, _ := context.WithCancel(context.Background())
	g := GrpcSink{}
	err := g.initConnect()
	if err != nil {
		log.Fatalf("client has error: %v", err)
		return
	}
	g.InitChan(ctx)
	s := time.Now().UnixNano() / 1e6
	for i := 0; i < 100000; i++ {
		g.gw.Add(1)
		event := &grpc_proto.Event{}
		resultMap := make(map[string]interface{})
		resultMap["id"] = fmt.Sprintf("%v%v", "Data", i)
		resultMap["Vt"] = 1
		resultMap["V"] = i
		resultMap["ts"] = time.Now().UnixNano() / 1e6
		// 测试生成token使用,后期需要调用方提供或者商讨正真的token生成规则
		resultMap["token"] = fmt.Sprintf("%v-%v", "token", i%len(g.chanMap))
		jsonStr, _ := json.Marshal(resultMap)
		event.Payload = string(jsonStr)
		headMap := make(map[string]string)
		headMap["stream.id"] = "InputStream"
		event.Headers = headMap
		g.chanMap[i%len(g.chanMap)] <- event
		//_, err := g.eventClient.Process(ctx, event)
		//if err != nil {
		//	return
		//}
		//go func() {
		//	_, err := g.eventClient.Process(ctx, event)
		//	if err != nil {
		//		log.Fatalf("grpc: sendData fail with error: %v \n ", err)
		//	}
		//	g.gw.Done()
		//}()
	}
	g.gw.Wait()
	e := time.Now().UnixNano() / 1e6
	log.Printf("耗时: [%v] ms,[%v] s \n", e-s, (e-s)/1000)
}
