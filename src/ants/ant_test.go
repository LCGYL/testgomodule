package ants

import (
	"github.com/panjf2000/ants/v2"
	"sync"
	"testing"
	"time"
)

func printFun() {
	time.Sleep(time.Duration(10) * time.Millisecond)
}

const (
	RunTimes           = 20000000
	BenchAntsSize      = 200000
	DefaultExpiredTime = 10 * time.Second
)

func BenchmarkGoroutines(b *testing.B) {
	var wg sync.WaitGroup
	b.StartTimer()
	for i := 0; i < b.N; i++ {
		wg.Add(RunTimes)
		for j := 0; j < RunTimes; j++ {
			go func() {
				printFun()
				wg.Done()
			}()
		}
		wg.Wait()
	}
	b.StopTimer()
}

func BenchmarkAntsPool(b *testing.B) {
	var wg sync.WaitGroup
	p, _ := ants.NewPool(BenchAntsSize, ants.WithExpiryDuration(DefaultExpiredTime))
	defer p.Release()

	b.StartTimer()
	for i := 0; i < b.N; i++ {
		wg.Add(RunTimes)
		for j := 0; j < RunTimes; j++ {
			_ = p.Submit(func() {
				printFun()
				wg.Done()
			})
		}
		wg.Wait()
	}
	b.StopTimer()
}
