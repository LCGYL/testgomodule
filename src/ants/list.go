package ants

import (
	"errors"
	"sync"
)

type List[T any] struct {
	data []T
	lock *sync.RWMutex
}

func (l *List[T]) InitList(lock *sync.RWMutex) {
	l.data = make([]T, 0)
	l.lock = lock
}
func (l *List[T]) Put(data T) {
	l.lock.Lock()
	defer l.lock.Unlock()
	l.data = append(l.data, data)
}
func (l *List[T]) Get(index int) (T, error) {
	if len(l.data) >= index {
		return l.data[index], nil
	} else {
		return nil, errors.New("out of range")
	}
}

func (l *List[T]) clear() {
	l.lock.RLock()
	l.lock.Lock()
	defer l.lock.RUnlock()
	defer l.lock.Unlock()
	l.data = []T{}
}
func (l *List[T]) Build() {
	l.lock.RLock()
	l.lock.Lock()
	defer l.lock.RUnlock()
	defer l.lock.Unlock()
	l.data = []T{}
}
