package algorithms

import "fmt"

/*
题目:
有 1、2、3、4 这四个数字，能组成多少个互不相同且无重复数字的三位数？都是多少？
思路:
可填在百位、十位、个位的数字都是 1、2、3、4。组成所有的排列后再去掉不满足条件的排列。
*/

//NumberPermutations 数字排列组合
func NumberPermutations() {
	total := 0
	// 三重循环 (三位数,四个数字)
	for i := 1; i < 5; i++ {
		for j := 1; j < 5; j++ {
			for k := 1; k < 5; k++ {
				if i != k && i != j && j != k {
					total++
					fmt.Printf("组合[%v]: i=%v,j=%v,k=%v =>%v%v%v \n", total, i, j, k, i, j, k)
				}
			}
		}
	}
	fmt.Printf("一共有[%v]种组合方式 \n", total)
}
