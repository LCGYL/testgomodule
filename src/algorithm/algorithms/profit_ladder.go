package algorithms

import "fmt"

/*
题目:
企业发放的奖金根据利润提成。
利润(I)低于或等于 10 万元时，奖金可提成 10%；
利润 10 万到 20 万元，低于 10 万元的部分按 10% 提成，高于 10 万元的部分，可提成 7.5%。
利润 20 万到 40 万之间时，高于 20 万元的部分，可提成 5%；
利润 40 万到 60 万之间时高于 40 万元的部分，可提成 3%；
利润 60 万到 100 万之间时，高于 60 万元的部分，可提成 1.5%，高于 100 万元时，超过 100 万元的部分按 1% 提成。

从键盘输入当月利润 I，求应发放奖金总数？
*/

//ProfitLadder 利润阶梯
func ProfitLadder() {
	i := 0.0
	bonus := 0.0
	fmt.Println("请输入利润[整数/小数]: ")
	_, err := fmt.Scanf("%f\n", &i)
	if err != nil {
		return
	}
	switch {
	case i > 1000000: // 100w
		bonus = (i - 1000000) * 0.01
		i = 1000000
		fallthrough
	case i > 600000: // 60w
		bonus += (i - 600000) * 0.015
		i = 600000
		fallthrough
	case i > 400000: // 40w
		bonus += (i - 600000) * 0.03
		i = 400000
		fallthrough
	case i > 200000: // 20w
		bonus += (i - 600000) * 0.05
		i = 200000
		fallthrough
	case i > 100000: // 10w
		bonus += (i - 600000) * 0.075
		i = 100000
		fallthrough
	default:
		bonus += i * 0.1
	}
	fmt.Printf("提成总计：%f\n", bonus)
}
