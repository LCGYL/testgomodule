package main

import "gitee.com/LCGYL/go-study/src/algorithm/algorithms"

func main() {
	testExactSquareNumber()
}

func testExactSquareNumber() {
	algorithms.ExactSquareNumber()
}

func testProfitLadder() {
	algorithms.ProfitLadder()
}

func testNumberPermutations() {
	algorithms.NumberPermutations()
}
