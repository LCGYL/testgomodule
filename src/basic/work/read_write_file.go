package work

import (
	"fmt"
	"io"
	"os"
)

func WorkRun() {
	write()
}

func write() {
	open, _ := os.OpenFile("/Users/liguoliang/Desktop/mydata/test/test2.txt", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0775)
	_, err := open.WriteString("hello")
	if err != nil {
		return
	}
	err = open.Close()
	if err != nil {
		return
	}
}

func read() {
	open, _ := os.Open("/Users/liguoliang/Desktop/mydata/test/test.txt")
	for {
		buf := make([]byte, 1024)
		n, err := open.Read(buf)
		if err == io.EOF {
			break
		}
		fmt.Printf("n: %v \n", n)
		fmt.Printf("string(buf): %v \n", string(buf))
	}
	err := open.Close()
	if err != nil {
		return
	}

}

func openClose() {
	file, _ := os.OpenFile("/Users/liguoliang/Desktop/mydata/test/test2.txt", os.O_RDWR|os.O_CREATE, 755)
	fmt.Printf("fileName: %v", file.Name())
	err := file.Close()
	if err != nil {
		return
	}
}
