package demo

import "fmt"

func StructRun() {
	var tom Person1
	fmt.Printf("tom: %v\n", tom)
	kite := Person2{}
	fmt.Printf("kite: %v\n", kite)

	struct1()
	struct2()
	struct3()
	struct4()
	struct5()
	struct6()
}

func struct6() {
	type Person struct {
		id, age     int
		name, email string
	}

	kite := Person{
		id:   1,
		name: "kite",
	}
	fmt.Printf("kite: %v\n", kite)
}

func struct5() {
	type Person struct {
		id, age     int
		name, email string
	}

	kite := Person{
		1,
		20,
		"kite",
		"kite@gmail.com",
	}
	fmt.Printf("kite: %v\n", kite)
}

func struct4() {
	type Person struct {
		id, age     int
		name, email string
	}

	kite := Person{
		id:    1,
		name:  "kite",
		age:   20,
		email: "kite@gmail.com",
	}
	fmt.Printf("kite: %v\n", kite)
}
func struct3() {
	type Person struct {
		id, age     int
		name, email string
	}

	var tom Person
	fmt.Printf("tom: %v\n", tom)
}
func struct2() {
	var dog struct {
		id   int
		name string
	}
	dog.id = 1
	dog.name = "花花"
	fmt.Printf("dog: %v\n", dog)
}
func struct1() {
	type Person struct {
		id, age     int
		name, email string
	}

	var tom Person
	tom.id = 1
	tom.name = "tom"
	tom.age = 20
	tom.email = "tom@gmail.com"
	fmt.Printf("tom: %v\n", tom)
}

type Person1 struct {
	id    int
	name  string
	age   int
	email string
}

type Person2 struct {
	id, age     int
	name, email string
}
