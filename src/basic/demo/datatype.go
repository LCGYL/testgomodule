package demo

import "fmt"

func aaa() {

}

func DataTypeRun() {
	name := "LCGYL"
	age := 20
	b := true
	fmt.Printf("name: %T,age: %T,b: %T \n", name, age, b)

	a := 100
	p := &a
	fmt.Printf("a: %T,p: %T \n", a, p)

	c := [3]int{1, 2, 3}
	fmt.Printf("c: %T \n", c)

	e := []int{1, 2, 3}
	fmt.Printf("e: %T \n", e)

	fmt.Printf("aaa(): %T \n", aaa)
}
