package demo

import "fmt"

func IfRun() {
	a := 1
	b := 2
	if a > b {
		fmt.Println(a)
	} else {
		fmt.Println(b)
	}
}
