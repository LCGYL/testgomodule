package demo

import (
	"bytes"
	"fmt"
	"strings"
)

func StringRun() {
	var str1 string = "hello world"
	var html string = `
	<html>
	<head><title>hello golang</title>
	</html>
`

	fmt.Printf("str1: %v\n", str1)
	fmt.Printf("html: %v\n", html)

	//字符串连接
	name := "tom"
	age := "20"
	msg := name + " " + age
	fmt.Printf("msg: %v\n", msg)
	fmt.Println("-------------")
	msg = ""
	msg += name
	msg += " "
	msg += age
	fmt.Printf("msg: %v\n", msg)

	// fmt.Sprintf()
	msg2 := fmt.Sprintf("%s,%s", name, age)
	fmt.Printf("msg: %v\n", msg2)

	// strings.Join()
	msg3 := strings.Join([]string{name, age}, ",")
	fmt.Printf("msg: %v\n", msg3)

	// buffer.WriteString()
	var buffer bytes.Buffer
	buffer.WriteString("tom")
	buffer.WriteString(",")
	buffer.WriteString("20")
	fmt.Printf("buffer.String(): %v\n", buffer.String())

	// 字符串转义字符
	fmt.Print("hello\tworld\n")
	fmt.Print("\"c:\\test\\\"")

	// 字符串切片操作
	str := "hello world"
	n := 3
	m := 5
	fmt.Println(str[n])   //获取字符串索引位置为n的原始字节
	fmt.Println(str[n:m]) //截取得字符串索引位置为 n 到 m-1 的字符串
	fmt.Println(str[n:])  //截取得字符串索引位置为 n 到 len(s)-1 的字符串
	fmt.Println(str[:m])  //截取得字符串索引位置为 0 到 m-1 的字符串

	// 字符串常用方法
	s := "hello world！"
	fmt.Printf("len(s): %v\n", len(s))
	fmt.Printf("strings.Split(s, \"\"): %v\n", strings.Split(s, " "))
	fmt.Printf("strings.Contains(s, \"hello\"): %v\n", strings.Contains(s, "hello"))
	fmt.Printf("strings.HasPrefix(s, \"hello\"): %v\n", strings.HasPrefix(s, "hello"))
	fmt.Printf("strings.HasSuffix(s, \"world！\"): %v\n", strings.HasSuffix(s, "world！"))
	fmt.Printf("strings.Index(s, \"l\"): %v\n", strings.Index(s, "l"))
	fmt.Printf("strings.LastIndex(s, \"l\"): %v\n", strings.LastIndex(s, "l"))
	fmt.Printf("strings.ToLower(s, \"l\"): %v\n", strings.ToLower("lWsA"))
	fmt.Printf("strings.ToUpper(s, \"l\"): %v\n", strings.ToUpper("lWsA"))

	// byte和rune类型
	var a = '华'
	var b = 'a'
	fmt.Printf("a: %v,%c\n", a, a)
	fmt.Printf("b: %v,%c\n", b, b)
}
