package demo

import "fmt"

func ArrayRun() {
	var a [3]int    // 定义一个int类型的数组a，长度是3
	var s [2]string // 定义一个字符串类型的数组s，长度是2

	fmt.Printf("a: %T\n", a)
	fmt.Printf("s: %T\n", s)

	var b [2]bool

	fmt.Printf("a: %v\n", a)
	fmt.Printf("s: %v\n", s)
	fmt.Printf("b: %v\n", b)

	e := [3]int{1, 2, 3}
	f := [2]string{"tom", "kite"}
	g := [2]bool{true, false}

	a1 := [2]int{1, 2} // 类型推断

	fmt.Printf("e: %v\n", e)
	fmt.Printf("f: %v\n", f)
	fmt.Printf("g: %v\n", g)
	fmt.Printf("a1: %v\n", a1)

	h := [3]int{1, 2, 3}
	i := [2]string{"tom", "kite"}
	j := [2]bool{true, false}

	a2 := [2]int{1, 2} // 类型推断

	fmt.Printf("h: %v\n", h)
	fmt.Printf("i: %v\n", i)
	fmt.Printf("j: %v\n", j)
	fmt.Printf("a2: %v\n", a2)

	k := [...]int{0: 1, 2: 2}
	l := [...]string{1: "tom", 2: "kite"}
	m := [...]bool{2: true, 5: false}

	a3 := [...]int{1, 2} // 类型推断

	fmt.Printf("k: %v\n", k)
	fmt.Printf("l: %v\n", l)
	fmt.Printf("m: %v\n", m)
	fmt.Printf("a3: %v\n", a3)
}
