package demo

import "fmt"

func IotaRun() {
	const (
		a1 = iota
		a2 = iota
		a3 = iota
	)
	fmt.Printf("a1: %v,a2: %v,a3: %v \n", a1, a2, a3)

	const (
		l1 = iota
		_
		l2 = iota
	)
	fmt.Printf("l1: %v,l2: %v \n", l1, l2)

	const (
		b1 = iota
		b2 = 100
		b3 = iota
	)
	fmt.Printf("b1: %v,b2: %v,b3: %v \n", b1, b2, b3)
}
