package demo

import "fmt"

func FunctionRecursionRun() {
	n := 5
	r := a(n)
	fmt.Printf("r: %v\n", r)

	r = f(5)
	fmt.Printf("r: %v\n", r)
}

func a(n int) int {
	// 返回条件
	if n == 1 {
		return 1
	} else {
		// 自己调用自己
		return n * a(n-1)
	}
}

func f(n int) int {
	// 退出点判断
	if n == 1 || n == 2 {
		return 1
	}
	// 递归表达式
	return f(n-1) + f(n-2)
}
