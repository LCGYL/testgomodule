package demo

import (
	"fmt"
	"testing"
)

func TestForBreak(t *testing.T) {
	for i := 0; i < 10; i++ {
		fmt.Printf("i:%v \n", i)
	ForJ:
		for j := 0; j < 10; j++ {
			switch j {
			case 1:
				break ForJ
			default:
				fmt.Printf("j:%v \n", j)
			}
		}
	}

}
