package demo

import "fmt"

func FunctionRun() {
	a := 5
	b := 6
	fmt.Printf("sum(5,6): %d \n", sum(a, b))
	fmt.Printf("compare(5,6): %d \n", compare(a, b))
	f1()
	n, i := f2()
	fmt.Printf("f2(): %s ,%d \n", n, i)
	s, age4 := f3()
	fmt.Printf("f3(): %s ,%d \n", s, age4)

	name4, age5 := f4()
	fmt.Printf("f4(): %s ,%d \n", name4, age5)

	f5(1, 2, 3)
	fmt.Println("------------")
	f5(1, 2, 3, 4, 5, 6)
	fmt.Println("------------")
	f6("tom", 20, 1, 2, 3)

	var f fun
	f = sum
	s2 := f(1, 2)
	fmt.Printf("s: %v\n", s2)
	f = max
	m := f(3, 4)
	fmt.Printf("m: %v\n", m)
}

func sum(a, b int) int {
	return a + b
}

func compare(a int, b int) (max int) {
	if a > b {
		max = a
	} else {
		max = b
	}
	return max
}
func max(a int, b int) int {
	if a > b {
		return a
	} else {
		return b
	}
}

func f1() {
	fmt.Printf("我没有返回值，只是进行一些计算 \n")
}

func f2() (name string, age int) {
	name = "老郭"
	age = 30
	return name, age
}

func f3() (name string, age int) {
	name = "老郭"
	age = 30
	return // 等价于return name, age
}

func f4() (name string, age int) {
	n := "老郭"
	a := 30
	return n, a
}

func f5(args ...int) {
	for _, v := range args {
		fmt.Printf("v: %v\n", v)
	}
}
func f6(name string, age int, args ...int) {
	fmt.Printf("name: %v\n", name)
	fmt.Printf("age: %v\n", age)
	for _, v := range args {
		fmt.Printf("v: %v\n", v)
	}
}

type fun func(int, int) int
