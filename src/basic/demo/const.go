package demo

import "fmt"

func ConstRun() {
	const Pi = 3.14
	fmt.Printf("Pi: %v \n", Pi)
	const (
		a = 100
		b = 200
	)
	fmt.Printf("a: %v,b: %v \n", a, b)
	const w, h = 200, 300
	fmt.Printf("w: %v,h: %v \n", w, h)
}
