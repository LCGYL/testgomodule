package demo

import "fmt"

func ForRangeRun() {
	a := [5]int{1, 2, 3, 4, 5}
	for i, v := range a {
		fmt.Printf("i: %d, v: %v\n", i, v)
	}

	s := "多课网，go教程"
	for i, v := range s {
		fmt.Printf("i: %d, v: %c\n", i, v)
	}

	s2 := []int{1, 2, 3, 4, 5}
	for i, v := range s2 {
		fmt.Printf("i, %d, v: %v\n", i, v)
	}

	m := make(map[string]string)
	m["name"] = "tom"
	m["age"] = "20"
	m["email"] = "tom@gmail.com"
	for k, v := range m {
		fmt.Printf("k: %v, v: %v\n", k, v)
	}

}
