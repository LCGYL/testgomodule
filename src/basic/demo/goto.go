package demo

import "fmt"

func GotoRun() {
	a := 1
	if a == 1 {
		goto LABEL1
	} else {
		fmt.Println("other")
	}

LABEL1:
	fmt.Println("next...")

	for i := 0; i < 5; i++ {
		for j := 0; j < 5; j++ {
			if i == 2 && j == 2 {
				goto LABEL2
			}
		}
	}
LABEL2:
	fmt.Println("label2")

}
