package demo

import "fmt"

func BoolRun() {
	var b1 bool = true
	var b2 bool = false

	var b3 = true
	var b4 = false

	b5 := true
	b6 := true
	println(fmt.Printf("b1: %v,b2: %v,b3: %v,b4: %v,b5: %v,b6: %v", b1, b2, b3, b4, b5, b6))

	age := 18
	if age >= 18 {
		fmt.Println("你已经成年")
	} else {
		fmt.Println("你还未成年")
	}
}
