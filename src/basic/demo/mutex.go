package demo

import (
	"fmt"
	"sync"
	"time"
)

var m int = 100

var lock sync.Mutex

var wt sync.WaitGroup

func MutexRun() {
	for i := 0; i < 100; i++ {
		go addMutex()
		wt.Add(1)
		go subMutex()
		wt.Add(1)
	}

	wt.Wait()
	fmt.Printf("m: %v\n", m)
}

func addMutex() {
	defer wt.Done()
	lock.Lock()
	m += 1
	time.Sleep(time.Millisecond * 10)
	lock.Unlock()
}

func subMutex() {
	defer wt.Done()
	lock.Lock()
	time.Sleep(time.Millisecond * 2)
	m -= 1
	lock.Unlock()
}
