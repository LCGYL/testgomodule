package demo

import (
	"fmt"
	"runtime"
	"time"
)

func RuntimeRun() {
	runtime1()
	runtime2()
	runtime3()
}

func runtime3() {
	fmt.Printf("runtime.NumCPU(): %v\n", runtime.NumCPU())
	runtime.GOMAXPROCS(2) // 修改为1查看效果
	go a1()
	go b1()
	time.Sleep(time.Second)
}

func runtime2() {
	go show3()
	time.Sleep(time.Second)
}

func runtime1() {
	go show2("java")
	// 主协程
	for i := 0; i < 2; i++ {
		// 切一下，再次分配任务
		runtime.Gosched() // 注释掉查看结果
		fmt.Println("golang")
	}
}

func show2(s string) {
	for i := 0; i < 2; i++ {
		fmt.Println(s)
	}
}

func show3() {
	for i := 0; i < 10; i++ {
		if i >= 5 {
			runtime.Goexit()
		}
		fmt.Printf("i: %v\n", i)
	}
}

func a1() {
	for i := 1; i < 10; i++ {
		fmt.Println("A:", i)
	}
}

func b1() {
	for i := 1; i < 10; i++ {
		fmt.Println("B:", i)
	}
}
