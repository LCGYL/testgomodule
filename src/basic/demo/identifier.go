package demo

import "fmt"

/*
1. 标识符由数字、字母和下划线(_)组成。123 abc _
2. 只能以字母和下划线(_)开头。abc123 _sysVar 123abc
3. 标识符区分大小写。 name Name NAME
4. demo
	var identifier type
	var：声明变量关键字
	identifier：变量名称
	type：变量类型

*/

// 正确的
var name string = "LCGYL"
var age int = 18
var _sys int = 18

// 类型推断
var name2 = "LCGYL2"
var age2 = 182
var _sys2 = 182

// 批量声明
var name3, age3, _sys3 = "LCGYL3", 183, 183

/*错误的
var 1name string
var &age int
var !email*/

func IdentifierRun() {
	// 短变量声明
	name4 := "LCGYL4"
	age4 := 184
	_sys4 := 184
	fmt.Printf("name: %s,age: %d,_sys: %d \n", name, age, _sys)
	fmt.Printf("name2: %s,age2: %d,_sys2: %d \n", name2, age2, _sys2)
	fmt.Printf("name3: %s,age3: %d,_sys3: %d \n", name3, age3, _sys3)
	fmt.Printf("name4: %s,age4: %d,_sys4: %d \n", name4, age4, _sys4)
}
