package demo

import "fmt"

func PointerRun() {
	pDemo1()
	pDemo2()
}

func pDemo1() {
	a := 20     /* 声明实际变量 */
	var ip *int /* 声明指针变量 */
	ip = &a     /* 指针变量的存储地址 */
	fmt.Printf("a 变量的地址是: %x\n", &a)
	/* 指针变量的存储地址 */
	fmt.Printf("ip 变量储存的指针地址: %x\n", ip)
	/* 使用指针访问值 */
	fmt.Printf("*ip 变量的值: %d\n", *ip)
}

const MAX int = 3

func pDemo2() {
	a := []int{1, 3, 5}
	var i int
	var ptr [MAX]*int
	fmt.Println(ptr) //这个打印出来是[<nil> <nil> <nil>]
	for i = 0; i < MAX; i++ {
		ptr[i] = &a[i] /* 整数地址赋值给指针数组 */
	}
	for i = 0; i < MAX; i++ {
		fmt.Printf("a[%d] = %d\n", i, *ptr[i]) //*ptr[i]就是打印出相关指针的值了。
	}
}
