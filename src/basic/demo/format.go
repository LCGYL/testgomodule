package demo

import "fmt"

type website struct {
	Name string
}

func FormatRun() {
	// 定义结构体变量
	site := website{Name: "local"}
	fmt.Printf("site: %v \n", site)
	fmt.Printf("site: %#v \n", site)
	fmt.Printf("site: %T \n", site)
	a := 100
	fmt.Printf("a: %T \n", a)
	fmt.Println("%%")
	b := true
	fmt.Printf("b: %t \n", b)
	fmt.Printf("1024: %b\n", 1024)         //二进制表示
	fmt.Printf("11111111: %c\n", 11111111) //数值对应的 Unicode 编码字符
	fmt.Printf("10: %d\n", 10)             //十进制表示
	fmt.Printf("8: %o\n", 8)               //八进制表示
	fmt.Printf("22: %q\n", 22)             //转化为十六进制并附上单引号
	fmt.Printf("1223: %x\n", 1223)         //十六进制表示，用a-f表示
	fmt.Printf("1223: %X\n", 1223)         //十六进制表示，用A-F表示
	fmt.Printf("1233: %U\n", 1233)         //Unicode表示
	fmt.Printf("12.34: %b\n", 12.34)       //无小数部分，两位指数的科学计数法6946802425218990p-49
	fmt.Printf("12.345: %e\n", 12.345)     //科学计数法，e表示
	fmt.Printf("12.34455: %E\n", 12.34455) //科学计数法，E表示
	fmt.Printf("12.3456: %f\n", 12.3456)   //有小数部分，无指数部分
	fmt.Printf("12.3456: %g\n", 12.3456)   //根据实际情况采用%e或%f输出
	fmt.Printf("12.3456: %G\n", 12.3456)   //根据实际情况采用%E或%f输出
	fmt.Printf("wqdew: %s\n", "wqdew")     //直接输出字符串或者[]byte
	fmt.Printf("dedede: %q\n", "dedede")   //双引号括起来的字符串
	fmt.Printf("abczxc: %x\n", "abczxc")   //每个字节用两字节十六进制表示，a-f表示
	fmt.Printf("asdzxc: %X\n", "asdzxc")   //每个字节用两字节十六进制表示，A-F表示
	//fmt.Printf("0x123: %p\n", 0x123)       //0x开头的十六进制数表示
}
