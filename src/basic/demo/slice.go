package demo

import "fmt"

func SliceRun() {
	var names []string
	var numbers []int
	fmt.Printf("names: %v\n", names)
	fmt.Printf("numbers: %v\n", numbers)
	fmt.Println(names == nil)
	fmt.Println(numbers == nil)

	var names2 = []string{"tom", "kite"}
	var numbers2 = []int{1, 2, 3}

	fmt.Printf("len: %d cap: %d\n", len(names2), cap(names2))
	fmt.Printf("len: %d cap: %d\n", len(numbers2), cap(numbers2))

	var s1 = make([]string, 2, 3)
	fmt.Printf("len: %d cap: %d\n", len(s1), cap(s1))

	sliceAdd()
	sliceDel()
	sliceCopy()
}

func sliceAdd() {
	var s1 []int
	s1 = append(s1, 1)
	s1 = append(s1, 2)
	s1 = append(s1, 3, 4, 5) // 添加多个元素
	fmt.Printf("s1: %v\n", s1)

	s3 := []int{3, 4, 5}
	s4 := []int{1, 2}
	s4 = append(s4, s3...) // 添加另外一个切片
	fmt.Printf("s4: %v\n", s4)
}

func sliceDel() {
	s1 := []int{1, 2, 3, 4, 5}
	// 删除索引为2的元素
	s1 = append(s1[:2], s1[3:]...)
	fmt.Printf("s1: %v\n", s1)
}

func sliceCopy() {
	s1 := []int{1, 2, 3}
	s2 := s1
	s1[0] = 100
	fmt.Printf("s1: %v\n", s1)
	fmt.Printf("s2: %v\n", s2)
	fmt.Println("----------")

	s3 := make([]int, 3)

	copy(s3, s1)

	s1[0] = 1

	fmt.Printf("s1: %v\n", s1)
	fmt.Printf("s3: %v\n", s3)
}
