package main

import (
	"fmt"
	demo2 "gitee.com/LCGYL/go-study/src/basic/demo"
	work2 "gitee.com/LCGYL/go-study/src/basic/work"
)

func main() {
	testPoolTest()
}

func testPoolTest() {
	work2.PoolTest()
}
func testWorkDemo() {
	work2.WorkDemo()
}
func testWorkRun() {
	work2.WorkRun()
}
func testFileRun() {
	demo2.FileRun()
}
func testMutexRun() {
	demo2.MutexRun()
}

func testRuntimeRun() {
	demo2.RuntimeRun()
}
func testGoroutinesWaitGroupRun() {
	demo2.GoroutinesWaitGroupRun()
}
func testGoroutinesChannelRun() {
	demo2.GoroutinesChannelRun()
}
func testGoroutinesRun() {
	demo2.GoroutinesRun()
}
func testStruct() {
	demo2.StructRun()
}
func testPointer() {
	demo2.PointerRun()
}

var a int = initVar()

func init() {
	fmt.Println("init")
}
func init() {
	fmt.Println("init2")
}

func initVar() int {
	fmt.Println("init var...")
	return 100
}

func testDefer() {
	demo2.DeferRun()
}

func testFunctionRecursionRun() {
	demo2.FunctionRecursionRun()
}
func testFunctionClosureRun() {
	demo2.FunctionClosureRun()
}
func testFunctionImproveRun() {
	demo2.FunctionImproveRun()
}
func testFunction() {
	demo2.FunctionRun()
}
func testMap() {
	demo2.MapRun()
}
func testSlice() {
	demo2.SliceRun()
}
func testArray() {
	demo2.ArrayRun()
}
func testGoto() {
	demo2.GotoRun()
}

func testForRange() {
	demo2.ForRangeRun()
}
func testOperator() {
	demo2.OperatorRun()
}
func testFormat() {
	demo2.FormatRun()
}
func testString() {
	demo2.StringRun()
}
func testInteger() {
	demo2.IntegerRun()
}
func testBoolRun() {
	demo2.BoolRun()
}
func testDataType() {
	demo2.DataTypeRun()
}
func testIota() {
	demo2.IotaRun()
}

func testConst() {
	demo2.ConstRun()
}

func testIdentifier() {
	demo2.IdentifierRun()
}
